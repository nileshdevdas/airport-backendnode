var express = require('express');
var router = express.Router();
var fs = require("fs");
var jwtHelper = require('../routes/jwthelper');
var daohelper = require('../routes/daohelper');
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.post('/api/login', (req, res) => {
    var response = {token: jwtHelper.createToken()};
    console.log(req.body.username);
    console.log(req.body.password);
    res.setHeader("content-type", "application/json");
    res.end(JSON.stringify(response));

});
/**
 * Now we ill get the airport from the Mongodb.......
 * http://localhost:3001/airports (But with a token )
 */
router.get("/api/airports", function (req, res, next) {
    daohelper.self.getAirports().then((result) => {
        res.send(result);
    });
});

module.exports = router;
