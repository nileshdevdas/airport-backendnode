var jwtHelper  = require('./jwthelper')
var express = require('express');
var router = express.Router();
/*
Write the token validation code and other here
 */
router.use(()=>{

})
/* GET users listing. */
router.get('/', function (req, res, next) {
    res.setHeader("content-type", "application/json")
    res.send({'username': 'nilesh', 'password': 'nilesh'});
});


router.post('/login', (req, resp) => {
    /**
     * Code to connect to the mongo db and other things .....
     * @type {{status: string, token: (undefined|*)}}
     */
    let result = {
        status: 'success',
        token: jwtHelper().createToken()
    }
    resp.setHeader("content-type", 'application/json');
    resp.end(JSON.stringify(result));
})
module.exports = router;
