const MongoClient = require("mongodb").MongoClient

/**
 * A helper api for the node js or you may call as a module.
 * @type {{getConnection: (function(): MongoClient), releaseConnection: releaseConnection, getAirports: (function(): *)}}
 */
const self = module.exports = {
    getConnection: async () => {
        const client = await MongoClient.connect('mongodb://localhost:27017');
        console.log(client);
        return client;
    },
    getAirports: async () => {
        const client = await self.getConnection();
        const db = client.db('airportsdb');
        const collection = db.collection("airports");
        const result = await collection.find({}).toArray();
        return result;
    }
    ,
    releaseConnection: async (connection) => {
        connection.close();
    }
}

module.exports = {
    self: self
}
