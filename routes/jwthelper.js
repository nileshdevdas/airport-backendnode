const jwt = require('jsonwebtoken');
module.exports = {
    createToken: () => {
        return jwt.sign({
            iat: new Date().getTime(),
            exp: new Date().getTime() + 180,
            issuer: 'nilesh',
            name: 'John Doe'
        }, 'veryverysecret');
    },
    validateToken: (token) => {
        jwt.verify(token, "veryverysecret")
    }
}
